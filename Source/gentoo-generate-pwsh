#!/bin/sh


# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2


set -e
export PATH
trap 'exit 128' INT


DOTNET_CLI_TELEMETRY_OPTOUT=1
export DOTNET_CLI_TELEMETRY_OPTOUT

DOTNET_NOLOGO=1
export DOTNET_NOLOGO

DOTNET_SKIP_FIRST_TIME_EXPERIENCE=1
export DOTNET_SKIP_FIRST_TIME_EXPERIENCE

MSBUILDDISABLENODEREUSE=1
export MSBUILDDISABLENODEREUSE

UseSharedCompilation=false
export UseSharedCompilation

POWERSHELL_TELEMETRY_OPTOUT=1
export POWERSHELL_TELEMETRY_OPTOUT

POWERSHELL_UPDATECHECK=0
export POWERSHELL_UPDATECHECK


DOTNET_COMPAT="${1}"

PV="${2}"

if [ -z "${PV}" ] ; then
    echo "ERROR: PV not given (is empty)"
fi


tmpdir="$(mktemp -d)"

mkdir -p "${tmpdir}"


build_dir="${tmpdir}/pwsh-${PV}"

pushd "${tmpdir}"
git clone                                                   \
    --branch "v${PV}"                                       \
    --depth 1                                               \
    --recursive                                             \
    --shallow-submodules                                    \
    https://github.com/PowerShell/PowerShell "pwsh-${PV}"


pushd "${build_dir}"

cache_dir="${build_dir}"/.cache

NUGET_PACKAGES="${cache_dir}"/nuget_packages
export NUGET_PACKAGES

echo " * DOTNET_COMPAT: ${DOTNET_COMPAT}"
echo " * NUGET_PACKAGES: ${NUGET_PACKAGES}"


[ -f "${build_dir}"/global.json ] &&
    rm "${build_dir}"/global.json

dotnet restore "${build_dir}"/src/powershell-unix
dotnet restore "${build_dir}"/src/ResGen
dotnet restore "${build_dir}"/src/TypeCatalogGen

cat > "${build_dir}/src/Microsoft.PowerShell.SDK/obj/Microsoft.PowerShell.SDK.csproj.TypeCatalog.targets" <<EOF
<Project>
  <Target Name="_GetDependencies"
     DependsOnTargets="ResolveAssemblyReferencesDesignTime">
    <ItemGroup>
      <_RefAssemblyPath Include="%(_ReferencesFromRAR.HintPath)%3B"
         Condition=" '%(_ReferencesFromRAR.NuGetPackageId)' != 'Microsoft.Management.Infrastructure' "/>
    </ItemGroup>
      <WriteLinesToFile File="\$(_DependencyFile)" Lines="@(_RefAssemblyPath)" Overwrite="true" />
  </Target>
</Project>
EOF

dotnet msbuild                                                                                          \
       "${build_dir}"/src/Microsoft.PowerShell.SDK/Microsoft.PowerShell.SDK.csproj                      \
       /t:_GetDependencies                                                                              \
       "/property:DesignTimeBuild=true;_DependencyFile=${build_dir}/src/TypeCatalogGen/powershell.inc"

echo "v${PV}" > "${build_dir}"/powershell.version

pushd "${build_dir}"/src/ResGen
dotnet run

pushd "${build_dir}"/src/TypeCatalogGen
dotnet run                                                                          \
       "${build_dir}"/src/System.Management.Automation/CoreCLR/CorePsTypeCatalog.cs \
       "${build_dir}"/src/TypeCatalogGen/powershell.inc

for obj_dir in bin obj ; do
    find "${build_dir}" -type d -name "${obj_dir}" -exec rm -r {} +
done

rm -r "${build_dir}"/.cache

rm -fr "${build_dir}"/.git


pushd "${tmpdir}"

tar --create --auto-compress --file "pwsh-${PV}.tar.xz" "pwsh-${PV}"

rm -r "${build_dir}"
