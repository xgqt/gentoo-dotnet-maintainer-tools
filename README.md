# Gentoo-Dotnet-Maintainer-Tools

Gentoo tool for .NET packages maintenance.

## Installation

### Repository

``` shell
meson setup build
cd build
meson install
```

In case you wnat to install to a lovcal image, then do:

``` shell
DESTDIR=$(pwd)/image meson install
```
